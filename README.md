# Fonctionne très bien avec Debian Stable

Je voulais installer Trinity Desktop sur debian stable. Ma recommandation est d'installer Debian stable minimal (pas d'interface graphique).

```
sudo apt install git

git clone https://github.com/tutodebian/debian-trinity

cd debian-trinity

sudo bash install.sh
```
