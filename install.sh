#!/bin/bash

# Configuration du gestionnaire de paquets
mv -f sources.list /etc/apt/sources.list

# Importation de la clé de signature GPG
wget http://mirror.ppa.trinitydesktop.org/trinity/deb/trinity-keyring.deb
dpkg -i trinity-keyring.deb

# Installation de Trinity desktop
apt update -y && apt install tde-trinity tde-i18n-fr-trinity -y

# Fin du script
reboot
